import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

# Load in data, so that this file can be ran 

filename = '../data/historical_data.csv'
df = pd.read_csv(filename)
df.columns

#------------------------------------------------------------------------------
# Concept checks in 2.1.1
#------------------------------------------------------------------------------


# Solution to Concept Check: Using `.value_counts` data to obtain a proportion:

vc = df['def_pay'].value_counts()
default_rate = vc[1]/vc.sum()
print(f'The baseline default rate is {default_rate}')


# Solution to Concept check: Use Boolean masks to get the default rate

pos_df = df[df['def_pay']==1]
neg_df = df[df['def_pay']==0]
default_rate = len(pos_df)/len(df)
print(f'The baseline default rate is {default_rate}')

# Alternative Solution to Concept check: Use Boolean masks to get the default rate

default_rate = df['def_pay'].sum() / df['def_pay'].count()
print(f'Alternative calculation: the baseline default rate is {default_rate}')


# Solution to Concept Check: Using summary data from .describe¶

# 'default' is coded as '1' and 'not default' is encoded as 0. 
# In this special case, it turns out to be the mean of this column: 

description_df = df.describe()
default_rate = description_df.loc['mean', 'def_pay']
print(f'The baseline default rate is {default_rate}')


# Solution to Concept check: Use .groupby result to find the proportion of elements with a given category

gb = df.groupby(['def_pay'])['def_pay']
s  = gb.count()

default_rate = s[1]/s.sum()
print(f'The baseline default rate is {default_rate}')

#------------------------------------------------------------------------------
# Concept checks in 2.1.2
#------------------------------------------------------------------------------

# Solution to Concept Check: Using DataFrame as report structure  

no_degree_df  = df[(df['EDUCATION'] == 3)]
has_degree_df = df[(df['EDUCATION'] == 1) | (df['EDUCATION'] == 2)]

no_degree_default_rate   = len(no_degree_df[no_degree_df['def_pay'] == 1])   / len(no_degree_df)
has_degree_default_rate  = len(has_degree_df[has_degree_df['def_pay'] == 1]) / len(has_degree_df)

summary_df = pd.DataFrame({'Has Degree':[has_degree_default_rate, 1-has_degree_default_rate], 
                           'No Degree':[no_degree_default_rate, 1-no_degree_default_rate]}, 
                           index=['Default', 'No default'])
print(summary_df)

# Solution to Concept check: Using the .groupby method to report on subgroup rates

gb = df.groupby('SEX')['def_pay'].mean()
gender_summary = pd.DataFrame({'Male':[gb[1], 1-gb[1]], 
                               'Female':[gb[2], 1-gb[2]]}, 
                              index=['Default', 'No Default'])
gender_summary
print(gender_summary)

#------------------------------------------------------------------------------
# Concept checks in 2.1.4
#------------------------------------------------------------------------------

# Solution to  Concept Check: Visualizing sub-group distributions

import matplotlib.pyplot as plt

# male_df stores only defaulting customers
male_df = df[df['SEX']==1]
# female_df stores only non-defaulting customers
female_df = df[df['SEX']==2]

fig, ax = plt.subplots()
hist_data = [male_df['LIMIT_BAL'], female_df['LIMIT_BAL']]
labels = ['male', 'female']
ax.hist(hist_data, bins=20, alpha=0.5, density=True, label=labels)
_ = ax.legend()


#------------------------------------------------------------------------------
# Concept checks in 2.1.5
#------------------------------------------------------------------------------

# Solution to Concept Check: calculating skew for sub-groups

from  scipy.stats import skew

male_skew   = skew(male_df['LIMIT_BAL'])
female_skew = skew(female_df['LIMIT_BAL'])
all_skew    = skew(df['LIMIT_BAL'])

# The following block creates the stats_df if it doesn't already exist
try:
    stats_df
except:
    stats_df = stats_df  = pd.DataFrame(columns=['Male', 'Female', 'All'])    
    
stats_df.loc['Skew',:] = [male_skew, female_skew, all_skew]
print(stats_df)

# Solution to Concept Check: Adding kurtosis statistic to the report 

from scipy.stats import kurtosis

kurt = df.groupby('SEX')['LIMIT_BAL'].agg(sts.kurtosis)

kurt.loc['All'] = kurtosis(df['LIMIT_BAL']) 
stats_df.loc['Kurtosis',:] = kurt.values
print(stats_df)

# Solution to Concept Check: reporting on percentile statistics

quantiles = [0.75, 0.9, 0.95, 0.99]
for quantile in quantiles:
    label    = f'{int(quantile*100)}th Percentile:'
    male_q   = np.quantile(male_df['LIMIT_BAL'], q=quantile)
    female_q = np.quantile(female_df['LIMIT_BAL'], q=quantile)
    all_q    = np.quantile(df['LIMIT_BAL'], q=quantile)
    
    stats_df.loc[label,:] = [male_q,female_q, all_q]
    
print(stats_df)
    
                            

