import matplotlib.pyplot as plt
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

# this means we get to see all columns:
pd.set_option('display.max_columns', None)


filename = '../data/historical_data.csv'
df = pd.read_csv(filename)
df.columns

# Solution to Concept check: Refactor the code so that any two months can be scatter-plotted

def plot_bill_vs_payment(df, month_index=1):

    fig, axarr = plt.subplots(1,2, figsize=(16,8))
    
    # pos_df stores only defaulting customers
    pos_df = df[df['def_pay']==1]
    # neg_df stores only non-defaulting customers
    neg_df = df[df['def_pay']==0]

    df_list = [pos_df, neg_df]
    colour_list = ['red', 'blue']
    title_list = ['DID default', 'did NOT default']
    log_scale = True
    alpha_list = [0.2, 0.05]
    for df_subset, ax, col, title,alpha in zip(df_list, 
                                     axarr, 
                                     colour_list, 
                                     title_list,
                                     alpha_list):
        df_subset.plot.scatter(f'BILL_AMT{month_index}', 
                               f'PAY_AMT{month_index}', 
                               ax=ax, 
                               logx = log_scale, 
                               logy = log_scale, 
                               c=col, 
                               title=title,
                               alpha=alpha)
        ax.set_xlim(50, 400000)
        ax.set_ylim(50, 400000)

plot_bill_vs_payment(df, 1)
plt.show(block=True)
# limits
# use log scale
# change default to red
# which is which? use titles


# Solution to Concept check: Tabulate correlation information

def get_corr(df, previous_month, sig):
    current_month = 1
    current_label = f'{sig}{current_month}'
    previous_label = f'{sig}{previous_month}'
    corr = df[current_label].corr(df[previous_label])
    return corr

stats_df = pd.DataFrame(columns = ['BILL_AMT', 'PAY_AMT'])

for month in range(2,7):
    result = []
    for sig in stats_df.columns:
        corr = get_corr(df, month, sig)
        result.append(corr)
    row_label = f'm1-m{month}'
    stats_df.loc[row_label, :] = result
print(stats_df)
stats_df.plot()
plt.show(block=True)