from flask import Flask, request

import insurance_broker as ib 
import json


broker = ib.InsuranceBroker(bid_offer_spread = 0.0)
model = ib.DefaultModel(prob_default = 0.22)


app = Flask(__name__)

@app.route('/')
def index():
    return {'Hello':'World!'}

@app.route('/request_quotes', methods=['POST'])
def request_quotes():
    tenders = request.json
    quotes = broker.get_quotes(tenders, model)
    return json.dumps(quotes)

@app.route('/process_orders', methods=['POST'])
def process_orders():
    orders = request.json
    order_summary = broker.process_orders(orders)
    return json.dumps(order_summary)



@app.route('/process_claims', methods=['POST'])
def process_claims():
    claims_summary = request.json
    claims_summary = broker.process_claims(claims_summary)
    return json.dumps(claims_summary)

if __name__ == '__main__':
    app.run(debug=True, port=5001)