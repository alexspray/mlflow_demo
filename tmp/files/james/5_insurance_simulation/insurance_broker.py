import copy
import numpy as np
import insurance_broker as ib 
from insurance_simulation import tender_columns
import json

class DefaultModel:
    """
    Currently prob_default is set at 0.22.
    Returns the probabilites of defaulting and the probabilities of not defaulting.
    """
    def __init__(self, prob_default):
        self.prob = prob_default

    def predict_proba(self, X):
        y_prob = np.ones((X.shape[0], 2))
        y_prob[:,1] *= (self.prob)  # default is '1' (second column)
        y_prob[:,0] *= (1-self.prob) # no default is '0' (first column)

        return y_prob 



class InsuranceBroker:

    def __init__(self, bid_offer_spread):
        self.spread = bid_offer_spread
        self.balance = 0

    def get_quotes(self, tenders, model):
        """
        Loops over all the tenders to get quotes according to the model
        """
        quotes = []
        for tender in tenders:
            quote = self.get_quote(tender, model)
            quotes.append(quote)
        return quotes

    def get_quote(self, tender, model):
        """
        Uses the model to calculate a quote from a tender.
        """
        X = self.get_feature_vector(tender['details'])
        prob_default_arr = model.predict_proba(X)
        prob_default = float(prob_default_arr[0,1])
        honest_premium = prob_default*tender['amount_insured']  # Amount the quotes are actually worth
        bid_premium = round(honest_premium*(1-self.spread/2)+0.005, 2)      # Making dolla
        offer_premium = round(honest_premium*(1+self.spread/2)-0.005, 2)

        quote = copy.deepcopy(tender)
        quote['bid_premium'] = bid_premium
        quote['offer_premium'] = offer_premium

        return quote

    def get_feature_vector(self, tender):
        """
        Uses tender columns to make a feature vector of all of those columns (Generates X).
        """

        feature_vector = np.zeros((1, len(tender_columns)))     # tender_columns imported at top of file
        for i, feature in enumerate(tender_columns):
            feature_vector[0,i] = tender[feature]
        return feature_vector

    def process_orders(self, orders):
        """
        """

        order_summary = {'initial_broker_balance':self.balance, 
                        'paid_to_broker':0, 
                        'paid_to_client':0, 
                        'net_payment_to_broker':0, 
                        'final_broker_balance':0}
        print(f"processing {len(orders)} orders")       
        for order in orders:
#            print(f"processing order {order['id']} for {order['type']} at {order['premium']}")
            premium = order['premium']
            if order['type']=='offer': # insured by broker
                order_summary['paid_to_broker'] += premium
            elif order['type']=='bid': # insured by client
                order_summary['paid_to_client'] += premium
            else:
                return json.dumps([f"Problem with order id {order['id']}"])  

        order_summary['net_payment_to_broker'] = order_summary['paid_to_broker'] - order_summary['paid_to_client']
        self.balance += order_summary['net_payment_to_broker']
        order_summary['final_broker_balance'] = self.balance 
        return order_summary

    def process_claims(self, claims_summary):
        
        claims_summary = copy.deepcopy(claims_summary)
        claims_summary['initial_broker_balance'] = self.balance
        self.balance -= claims_summary['net_claim_from_broker']
        claims_summary['final_broker_balance'] = self.balance
        return claims_summary
