from waitress import serve
from flask import Flask, request
from sklearn.linear_model import ElasticNet
import json
model = ElasticNet(alpha=0.05, l1_ratio=0.15)
# TODO: make mlflow client, load chosen model from mlflow server

app = Flask (__name__)

@app.route('/predict', methods=['POST'])
def predict():
    feature_data = request.json
    print(f'there are {len(feature_data)} elements in the list')
    return json.dumps(['no', 'predictions', 'yet.', 'sorry'])


serve(app, port=5001, host="0.0.0.0")
