import requests
import pandas as pd
from sklearn.model_selection import train_test_split
# load a sample of wine feature data

data_uri = r'file:..\data\wine_quality.csv'
df = pd.read_csv(data_uri, index_col=0)


print(df.head())

df_train, df_test = train_test_split(df, test_size=0.2, random_state=123)

target = 'quality'
Y_train = df_train[[target]]
Y_test = df_test[[target]]
X_train = df_train.drop(columns=target)
X_test = df_test.drop(columns=target)

to_send_df = X_test.sample(5, random_state=123)
print(to_send_df)
json_list = []
for row in to_send_df.index:
    print('row', row)
    item = to_send_df.loc[row,:].to_dict()
    json_list.append(item)
print('list length is ', len(json_list))
print(json_list[-1])

# compose the features as a json payload to send 
# as a POST request to wine prediction server

host = 'http://127.0.0.1:5001/'
end_point = 'predict'
url = host+end_point

# make the request to the server

response = requests.post(url, json=json_list)

# process the response: print out the nearest prediction 
# and the most wayward prediction
if response.status_code<300:
    print('response successfully received')
    print(response.text)
else:
    print('error', response.status_code)
