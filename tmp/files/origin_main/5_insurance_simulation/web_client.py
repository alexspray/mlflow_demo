import insurance_client as ic
from insurance_simulation import tender_columns
import pandas as pd

def get_summary_row(row_label, order_summary, claims_summary, include_bid_trades=False):

    norm_premiums = order_summary['paid_to_broker']/order_summary['client_total_order']

    col_dict = {'Inital Client Balance:':[order_summary['initial_client_balance']],
                'Inital Broker Balance:':[order_summary['initial_broker_balance']],
                'Orders Placed by Client:':[order_summary['client_total_order']],
                'Premiums Paid by Client:':[order_summary['paid_to_broker']],
                'Claims Made by Client:':[claims_summary['claimed_from_broker']],
                'Normalised Client Premiums:':norm_premiums}

    net_to_broker = order_summary['paid_to_broker'] - claims_summary['claimed_from_broker']
    total_volume = order_summary['client_total_order']
    if include_bid_trades:
        if order_summary['broker_total_order']>0:
            norm_premiums = order_summary['paid_to_client']/order_summary['broker_total_order']
        else:
            norm_premiums = 0
        net_to_broker -= (order_summary['paid_to_client']-claims_summary['reimbursed_to_broker'])
        total_volume += order_summary['broker_total_order']
        col_dict.update( {
            'Orders Placed by Broker:':[order_summary['broker_total_order']],
            'Premiums Paid by Broker:':[order_summary['paid_to_client']],
            'Claims Made by Broker:':[claims_summary['reimbursed_to_broker']], 
            'Normalised Broker Premiums:':norm_premiums,
        })
    norm_to_broker =  net_to_broker/total_volume
    col_dict.update({
        'Normalized transfer:':[norm_to_broker],        
        'Final Client Balance:':[claims_summary['final_client_balance']],
        'Final Broker Balance:':[claims_summary['final_broker_balance']],
        })


    df = pd.DataFrame(col_dict, index=[row_label])
    return df





claims_columns = ['ID', 'def_pay']



df = pd.read_csv(r'..\data\historical_data.csv')
num_tenders = 10
number_of_sims = 5


client = ic.InsuranceClient()

broker_uri = 'http://127.0.0.1:5000/'

summary_df = []

for sim in range(number_of_sims):
    sample_df = df.sample(num_tenders)
    tenders_df = sample_df[['ID']+ tender_columns]
    claims_df = sample_df[claims_columns]

    order_summary, claims_summary = client.buy_and_use_insurance(tenders_df, claims_df, broker_uri)

    summary_row = get_summary_row(sim, order_summary, claims_summary)
    if len(summary_df)==0:
        summary_df = summary_row
    else:
        summary_df = pd.concat([summary_df, summary_row])

summary_df.to_html('summary.html')




    