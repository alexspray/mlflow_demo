from insurance_simulation import tender_columns
import requests
import copy

class InsuranceClient:

    def __init__(self):
        self.balance = 0

    def buy_and_use_insurance(self, tenders_df, claims_df, broker_uri):
        
        quotes = self.get_quotes(tenders_df, broker_uri)

        orders = self.get_orders_from_quotes(quotes)

        order_summary = self.execute_orders(orders, broker_uri)

        claims_summary = self.process_claims(orders, claims_df, broker_uri)

        return order_summary, claims_summary

    def get_quotes(self, tenders_df, broker_uri):
        
        tenders = []
        for row in tenders_df.index:
            details = {feature:float(tenders_df.loc[row, feature]) for feature in tender_columns}
            tender = {'id': str(tenders_df.loc[row, 'ID']), 
                      'amount_insured': float(tenders_df.loc[row, 'BILL_AMT1']), 
                      'details': details}
            tenders.append(tender)
        url = broker_uri + 'request_quotes'
        response = requests.post(url, json=tenders)
        if response.status_code>=300:
            raise Exception(f'get_quotes raised {response.status_code} - sorry')
        quotes = response.json()
        return quotes


    def get_orders_from_quotes(self, quotes):

        return self.get_orders_for_offers(quotes)


    def get_orders_for_offers(self, quotes): # buy all quoted insurance

        orders = []
        for quote in quotes:
            order = copy.deepcopy(quote)
            order['type'] = 'offer'
            order['premium'] = quote['offer_premium']
            orders.append(order)
        return orders


    def execute_orders(self, orders, broker_uri):

        initial_client_balance = self.balance
        total_offer = 0
        client_total_order = 0
        broker_total_order = 0
        paid_to_broker = 0
        paid_to_client = 0
        for order in orders:
#            print(order)
            if order['type']=='offer':
                client_total_order += order['amount_insured']
                paid_to_broker += order['premium']
            elif order['type']=='bid':
                broker_total_order += order['amount_insured']
                paid_to_client += order['premium']
        self.balance += paid_to_client - paid_to_broker        
        final_client_balance = self.balance
        
        url = broker_uri + 'process_orders'
#        print('the orders:', orders)

        response = requests.post(url, json=orders ) 
        if response.status_code>=300:
            raise Exception(f'execute_orders() from {url}, ' 
                      + f'status code:{response.status_code}, '
                      + f'reason:{response.reason}')   

        order_summary = response.json()
        order_summary['broker_total_order'] = broker_total_order
        order_summary['client_total_order'] = client_total_order
        order_summary['initial_client_balance'] = initial_client_balance
        order_summary['final_client_balance'] = final_client_balance
        order_summary['paid_to_broker'] = paid_to_broker
        order_summary['paid_to_client'] = paid_to_client

        return order_summary




    def process_claims(self, orders, claims_df, broker_uri):
        claims_df = claims_df.set_index('ID')
        claims_summary = {'initial_client_balance':self.balance, 
                   'claimed_from_broker':0, 
                   'reimbursed_to_broker':0, 
                   'net_claim_from_broker':0, 
                   'final_client_balance':0}
        
        for order in orders:
            id = int(order['id'])   
            default_status = claims_df.loc[id, 'def_pay']
            default_status = int(default_status)
            if default_status:
                if order['type']=='bid': # insured by client
                    claims_summary['reimbursed_to_broker'] += order['amount_insured']
                elif order['type']=='offer': # insured by broker
                    claims_summary['claimed_from_broker'] += order['amount_insured']
                else:
                    raise Exception(f"unexpected type of order for id {order['id']}")
        claims_summary['net_claim_from_broker'] = (claims_summary['claimed_from_broker'] 
                                                 - claims_summary['reimbursed_to_broker'])

        self.balance += claims_summary['net_claim_from_broker']                                                   

        claims_summary['final_client_balance'] = self.balance 

        url = broker_uri + 'process_claims'
        response = requests.post(url, json=claims_summary) 
        if response.status_code<=300:
            claims_summary = response.json()
            return claims_summary
        raise Exception(f'process_claims() from {url}, ' 
                      + f'status code:{response.status_code}, '
                      + f'reason:{response.reason}')   





