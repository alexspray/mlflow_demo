# MLflow tracking

The `mflow ui` program starts a web service (on by default `127.0.0.1:5000`), and reports on experiments found in the specified folder (by default, `mlruns`)

## First part:

Lets write an experiment to create and execute some 'runs' that evaluate performance on an ML model with specified hyper-parameters.

- model to be tested: `ElasticNet`
- hyperparameters: `alpha`, `l1_ratio`
- type of optimization: random sampling


- make a parent  folder to hold the experiments
- make a sub-folder called `mlruns`
- from the parent folder, run `mlflow ui`
- check that the `uri` in the python source is the same as the sub-folder path.
- run the python program to generate some runs using `python local_tracking.py --n 100` 


- log the model to be saved in a state that allows context-free loading
- log tags associated with specific runs
- we can also enable auto-logging for models from specific packages eg sklearn, `mlflow.sklearn.autolog`

## Choose a model and load it in:


- search the results using the `search_runs` function:
    - returns a df, in this case we can use use df['metrics.mae_test'].idxmin() to find the index with the lowest error.
- search the results by creating a `mlflow.tracking.MlflowClient` and use its methods to obtain results and models 
- both these methods work with different architectures of apparatus:
    - experiments logged and served via local folder (uri)
    - experiments logged and served via restful access to localhost server (uri)
    - experiments logged and served via restful access to externally visible server (uri)

- 'remote' logging of results



## HyperOpt optimization

The hyperopt package allows us to model the estimator score as a function of the hyper-parameters, using a tree of parzen window estimators.
- By itself, it provides a function `fmin` that will minimize a function, given some objective scoring function.
- There are supplementary libraries that allow hyperopt to be used with families of ML models, e.g. sklearn uses hyperopt-sklearn
-  Unfortunately, the main branch of hyperopt-sklearn is broken, bcause it relies on a legacy version of numpy random number generation 
- however, the [merge request](https://github.com/rharish101/hyperopt-sklearn/tree/numpy_generator) fixes this, but we need to download the repo and install from the folder (using `pip install .`)
- you should then be able to run the notebook `hyperopt_demo.ipynb`


