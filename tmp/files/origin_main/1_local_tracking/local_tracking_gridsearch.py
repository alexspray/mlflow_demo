
tracking_uri    = r'file:C:\tmp\MLE04\mlruns'
experiment_name = 'Wine Quality - Grid Search'

import numpy as np
import pandas as pd
import mlflow
import argparse

from sklearn.linear_model import ElasticNet
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split


parser = argparse.ArgumentParser()
parser.add_argument('--max_alpha', type=float, default=10, help='max value of alpha')
parser.add_argument('--n', type=int, default=3, help='number of runs')

args = parser.parse_args()

print(args.n)

#data_uri = r'http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv'
#df = pd.read_csv(data_uri, sep=';')
data_uri = r'file:..\data\wine_quality.csv'
df = pd.read_csv(data_uri, index_col=0)


print(df.head())

df_train, df_test = train_test_split(df, test_size=0.2, random_state=123)

target = 'quality'
Y_train = df_train[[target]]
Y_test = df_test[[target]]
X_train = df_train.drop(columns=target)
X_test = df_test.drop(columns=target)

print('X_train', X_train.shape)
print('X_test', X_test.shape)
print('Y_train', Y_train.shape)
print('Y_test', Y_test.shape)

#df.to_csv('local_copy.csv')

def evaluate_model(Y_real, Y_pred):
    mse = mean_squared_error(Y_real, Y_pred)
    mae = mean_absolute_error(Y_real, Y_pred)
    r2 = r2_score(Y_real, Y_pred)
    return mse, mae, r2

# where we will track the experiments:
mlflow.set_tracking_uri(tracking_uri)
# name of the experiment:
mlflow.set_experiment(experiment_name)

number_of_experiments = args.n

from sklearn.model_selection import GridSearchCV
parameters = {'alpha':   [0.02, 0.05, 0.09], 
              'l1_ratio':[0.03, 0.06, 0.10]}

clf = GridSearchCV(ElasticNet(), parameters, cv=5)
clf.fit(X_train, Y_train)

best_alpha = clf.best_estimator_.alpha
best_l1_ratio = clf.best_estimator_.l1_ratio

print(clf.best_estimator_)

with mlflow.start_run():
    mlflow.log_param('alpha', best_alpha)
    mlflow.log_param('best_l1_ratio', best_l1_ratio)
    mlflow.sklearn.log_model(clf.best_estimator_)