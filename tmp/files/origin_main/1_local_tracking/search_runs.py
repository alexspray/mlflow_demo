import mlflow
from pkg_resources import run_script
import os

tracking_uri = r'file:../mlruns'
experiment_name = 'Wine Quality - Elastic Net'



client = mlflow.tracking.MlflowClient(tracking_uri=tracking_uri)

experiment_id = client.get_experiment_by_name(experiment_name).experiment_id
#input(experiment_id)
runs          = client.search_runs(experiment_id, order_by=['metrics.mae_test'])

best_run_id = runs[0].info.run_id
print('from client: best run id is ', best_run_id)

download_folder = "../downloaded_model"

client.download_artifacts(best_run_id, 'model', download_folder)

model_uri = 'file:' + os.path.join(download_folder, 'model')
model = mlflow.sklearn.load_model(model_uri)
print(model)


def get_df():

    mlflow.set_tracking_uri(tracking_uri)
    df = mlflow.search_runs()
    print(df.columns)
    print(df)
    idx = df['metrics.mae_test'].idxmin()
    print (df.loc[idx,:])

    run_id = df.loc[idx,'run_id']

    print('from df: best run id is', run_id)
