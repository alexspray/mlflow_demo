
tracking_uri    = r'file:../mlruns'
experiment_name = 'Wine Quality - Elastic Net'

import numpy as np
import pandas as pd
import mlflow
import argparse

from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split


parser = argparse.ArgumentParser()
parser.add_argument('--max_alpha', type=float, default=10, help='max value of alpha')
parser.add_argument('--n', type=int, default=3, help='number of runs')

args = parser.parse_args()

print(args.n)

#data_uri = r'http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv'
#df = pd.read_csv(data_uri, sep=';')
data_uri = r'file:..\data\wine_quality.csv'
df = pd.read_csv(data_uri, index_col=0)


print(df.head())

df_train, df_test = train_test_split(df, test_size=0.2, random_state=123)

target = 'quality'
Y_train = df_train[[target]]
Y_test = df_test[[target]]
X_train = df_train.drop(columns=target)
X_test = df_test.drop(columns=target)

print('X_train', X_train.shape)
print('X_test', X_test.shape)
print('Y_train', Y_train.shape)
print('Y_test', Y_test.shape)

#df.to_csv('local_copy.csv')

def evaluate_model(Y_real, Y_pred):
    mse = mean_squared_error(Y_real, Y_pred)
    mae = mean_absolute_error(Y_real, Y_pred)
    r2 = r2_score(Y_real, Y_pred)
    return mse, mae, r2

# where we will track the experiments:
mlflow.set_tracking_uri(tracking_uri)
# name of the experiment:
mlflow.set_experiment(experiment_name)

number_of_experiments = args.n
for exp_index in range(number_of_experiments):
    with mlflow.start_run(): # context manager -- keeps experiment open only in this block
        # Get hyper-parameters:
        min_impurity_decrease  = np.random.uniform(0, 0.1)
        min_samples_leaf = np.random.uniform(0, 0.05)
        print(f'starting exp with min_impurity_decrease {min_impurity_decrease} and min_samples_leaf of {min_samples_leaf}')
        mlflow.log_param('min_impurity_decrease', min_impurity_decrease)
        mlflow.log_param('min_samples_leaf', min_samples_leaf)

        model = DecisionTreeRegressor(min_samples_leaf = min_samples_leaf, min_impurity_decrease = min_impurity_decrease)
        model.fit(X_train, Y_train)
        
        mse, mae, r2 = evaluate_model(model.predict(X_test), Y_test)
        mlflow.log_metric('mse_test', mse)
        mlflow.log_metric('mae_test', mae)
        mlflow.log_metric('r2_test', r2)

        mse, mae, r2 = evaluate_model(model.predict(X_train), Y_train)
        mlflow.log_metric('mse_train', mse)
        mlflow.log_metric('mae_train', mae)
        mlflow.log_metric('r2_train', r2)

        mlflow.sklearn.log_model(model, 'model')
        

