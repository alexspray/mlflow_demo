# Insurance Broker

Your task is to design, implement, deploy and trial software components for creating and processing insurance quotes for credit default. 

Part of the challenge is to develop models that predict as accurately as possible whether a given customer profile will 'default on' (not pay) their  next credit card bill. 

Another part of the challenge is to use this prediction in a business context. 

The final (and arguably the most important) aspect is to deploy the components in a simulated trading environment, either running in a local containerised execuation environment (laptop) or a cloud container resource.   

## Challenges:

Here are some objectives for your team to complete:

1. Investigate ML models to predict customer default. Use the dataset `historical_data.csv` to train and test your models. 

2. What's the lowest fixed-rate premium that a broker can offer to insure at, and still make a profit?  (A fixed-rate of premium per-dollar-insured, not taking into account any features of the customer)

3. What's the lowest average premium that a broker can offer to insure at, and still make a profit? (Taking account of customer details ) -- average premium per dollar insured.

4. Deploy your Broker as a Docker Image and provide a demonstration of it being deployed as a container and providing quotes, processing orders and claims.  

5. Make your Broker service available as a docker image that can be pulled and used by other teams in bid/offer insurance simulations. They will provide the client

5. Pull another team's broker service,  service available as a docker image that can be pulled and used by other teams in bid/offer insurance simulations. They will provide the client 

6. Help organize a symmetric competition in which different brokers buy and sell insurance, exchanging money for the premiums and then again for the claims





## Scenario

Insurance brokers are invited to 'offer' to insure a customer's credit against default. This means that, if the customer defaults on their next payment (for the current bill amount), then the insured party (e.g. the credit card company) can submit a claim to be reimbursed (by the broker) for the current bill amount.


### General workflow for insurance quotes, purchases and claims

First exchange: 
- the client requests quotes for insurance, by providing  'tenders', which include id, 'amount_to_insure' and 'customer details'.
- broker responds with 'bid' and 'offer' premiums for each customer profile.

Second exchange: place orders, confirm orders


Third exchange: settle claims, confirm settlement






### Symmetrical trading: brokers that offer insurance and also bid to be insured





- The *bid* price or `bid_premium` represents the maximum price that an insurance buyer is willing to pay, to insure a current balance against default, for a customer who fits a given  
- The *offer* price or `offer_premium` represents the minimum price that an insurance seller is willing to take, in order to insure against credit default.
- Invariably, the offer price is higher than the bid price: the difference is called the 'bid-offer spread'.

## Data

To assist the broker in deciding what would be a suitable premium to charge, a historical dataset is made available in csv format.

Interpretation of the columns is as follows:

The target variable is a binary variable, `def_pay` ('default on next payment', Yes = 1, No = 0)

- `LIMIT_BAL`: Amount of the given credit (Taiwainese dollars): it includes both the individual consumer credit and his/her family (supplementary) credit.
- `SEX`: Gender (1 = male; 2 = female).
- `EDUCATION`: Education (1 = graduate school; 2 = university; 3 = high school; 4 = others).
- `MARRIAGE`: Marital status (1 = married; 2 = single; 3 = others).
- `AGE`: Age (year).
- `PAY_1...6`: History of past payment. We tracked the past monthly payment records (from April to September, 2005) as follows: `PAY_1` = the repayment status in September, 2005; `PAY_2` = the repayment status in August, 2005; . . .;`PAY_6` = the repayment status in April, 2005. The measurement scale for the repayment status is: -1 = pay duly; 1 = payment delay for one month; 2 = payment delay for two months; . . .; 8 = payment delay for eight months; 9 = payment delay for nine months and above.
- `BILL_AMT1...6`: Amount of bill statement (NT dollar). X12 = amount of bill statement in September, 2005; X13 = amount of bill statement in August, 2005; . . .; X17 = amount of bill statement in April, 2005.
- `PAY_AMT1`: Amount of previous payment (NT dollar). `PAY_AMT1` = amount paid in September, 2005; `PAY_AMT2`= amount paid in August, 2005; . . .;`PAY_AMT6` = amount paid in April, 2005.


## Workflow


1. The client makes a POST request to the `request_for_quote` endpoint, providing a list with each element containing `customer_id`,  `amount_to_insure` and `customer_features` data 
2. The broker responds with a list of quotes, each element containing 'offer' and 'bid' insurance premiums: 
    - The 'offer' of insurance for the client means that:
        - The client pays the offer_premium to the broker now
        - If the customer_id defaults, the broker pays the client the amount_to_insure )
    - The 'bid' for insurance by the client means that:
        - The broker pays the bid_premium to the client now
        - If the customer_id defaults, the client pays the broker the amount_to_insure )
    - The quoted 'offer' premium must be at most 2% higher than the 'bid' premium 

3. client sends purchase_order containing customer_id, amount_to_insure, bid/offer, and quoted price (either the bid_premium or the offer_premium)

3. broker (and client) keep track of the money that changes hands (bid_premiums are paid to the client, offer_premiums are paid to the broker.)

4. client receives information about which customers actually have defaulted, grouped into two: 
    - The first customer group is those who the client bid to insure for the broker: their insured amounts are added up to be re-embursed to the broker. 
    - The second customer group is those who the broker offered to insure for the client:  their insured amounts are added up to be claimed from the broker. 

5. The client sends a claim containing: 
    - The reimbursement_amount
    - The claim_amount
    - The balance (negative balance means a net reimbursement, i.e. a payment from client
    
    )

## Example

6. Example: (initially, let's set the client and broker balances both at £0 each)
    - client sends a requestForQuote containing 4 customers, each with a cusomer_id, an  amount_to_insure:
        - id 101: amount £500, features indicate low risk
        - id 102: amount £1000, features indicate high risk
        - id 103: amount £2000, features indicate high risk
        - id 104: amount £3000, features indicate low risk
    - the broker responds with the following bid/offers:
        - id 101: bid £50,offer £51 
        - id 102: bid £200, offer £204
        - id 103: bid £400, offer £408
        - id 104: bid £300, offer £306

    - the client sends a purchase_order that contains the following:
        - id 101: client bids to insure this customer: enclosed £50 invoice for broker to pay   
        - id 102: client accepts broker's offer to insure this customer: enclosed remittence of £204 
        - id 103: client bids to insure this customer: enclosed £400 invoice for broker to pay   
        - id 104: client accepts broker's offer to insure this customer: enclosed remittence of £306
        - includes the balance of £204 + £306 - £50 - £400 = £510 - £450 = £60
        - client balance is now -£60, the broker balance is now £60
     
    - the client is informed (from elsewhere) that customers 101 and 104 defaulted, and settles the insurance claims with the broker as follows:
        - id 101 is insured by the client, so £500 will be transferred from client to broker
        - id 104 is insured by the broker, so £3000 will be transferred from the broker to the client. 
        - The net amount of the above two is £2500 (to be transferred from broker to client.)
        - The client's balance is now £2440, and the broker's balance is now -£2440 


