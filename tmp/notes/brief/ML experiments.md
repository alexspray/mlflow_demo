
# Model selection, tuning, verification and updating

all the above require an evaluation of ML models against some verification / test set


At project outset:

- Selection:comparing Model types A, B and C  (eg Logistic Regression, Decision Tree Classifer, KNN)  
    - we would explore some space of hyper parameters for each model type
- Moding tuning: for a give model type, what is best hyper-parameter config

During project operation:

- Verify that the model / system is working acceptably
- Training on updated data (with / without variation of hyper-parameters)



## Hyper-parameter selection:

### why is this difficult?

- Curse of dimensionality
- Non-convex optimization
- Computational cost
- Non-intuitive hyperparameters


### Search for hyperparameters:

- Manual Selection
- Grid Search 
- Random Search 
- Population-based methods (fireworks, genetic algorithms)
- Bayesian methods (explicitly modelling the model fitness as a function of the Hyper-parameters, e.g. hyperopt)



