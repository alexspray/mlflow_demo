# Hosting a Docker container on Azure VM

The following steps show how to host a Docker container running a Flask app serving your ML model on an Azure Virtual Machine (VM). It is assumed that you have already created a Docker image for your Flask app and pushed it to a registry (e.g. DockerHub).

Note: This example uses free tier eligible services such as a basic B1s instance which are available for free for 12 months after creating the Azure account (with some restrictions/limits). You get a $200 credit for 30 days to use after which certain services are free for 12 months. For more information, check on the Azure website.

Always remember to stop the VM through Azure after finishing working on it and check if it is in a "Stopped (deallocated)" state to ensure that you won't be charged by the hour for the instance. 

## Creating the Azure VM instance

- Go to the [Azure Portal](portal.azure.com)
- Click on Virtual Machines under Azure services
- Click on Create and select Virtual Machine
- Set options as per below and leave the rest to their default values
- In the Basics tab:
  - Choose a name and region (with free service eligibility) for the VM
  - Select 'Ubuntu Server 20.04 LTS - Gen2' under Image
  - Select 'Standard_B1s' under Size
  - Select SSH public key for Authentication type
  - Select 'Generate new key pair' for SSH public key source if you don't already have one and create a key pair name
- Click on Next: Disks
  - Choose a P6 (64GiB) or lower size as OS disk size
- Click on Next: Networking
  - Under NIC network security group, select Advanced.
  - Click on Create new under Configure network security group.
    - In the Create network security group window, click on 'Add an inbound rule'
    - Select a port number (the port where your Flask app will be running on the Docker container) for the Destination port range, for example, 13746
    - Select TCP as Protocol
    - Set Name as Port_13746
    - Click on Add
- Leave the defaults for Management, Advanced and Tags tab and go to Review & Create tab
  - Click on Create at the bottom of the page. This will open a popup window for generating new key pair.
  - Click on 'Download private key and create resource'. Save the *.pem file in your computer. 
  - On the Azure portal, check that the VM has started. Note the Public IP address under Networking in the Overview Tab.

## Connect to the VM using SSH

Open a PowerShell window (Windows) or Terminal (Mac) and type the following:

`ssh -i <path_to_pem_file> azureuser@<public_ip_of_AzureVMinstance>`

If you see any errors regarding permission issues around the pem file (e.g. UNPROTECTED PRIVATE KEY FILE), change the permissions for the file accordingly to be read only for the current user. 

On Windows, do the following:

- Right click on the pem file
- Go to security > advanced
- Disable inheritance and remove all permissions
- Click on Add > Select principal > Advanced > Find Now
- Select your user
- Give permissions

On Mac, type `chmod 400 <path_to_pem_file>` in Terminal.

## Install Docker on the Linux VM

Once connected to the VM, follow the instructions [here](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository) to install the Docker engine.

Run the docker hello world image to make sure it is correctly installed.

## Set up the Docker container with the Flask app on the VM

Pull the image from DockerHub into the VM using the `docker pull` command.

`docker pull <dockerhub_username>/<flaskapp_imagename>`

Run the image using the `docker run` command.

`docker run -p 5000:13746 <dockerhub_username>/<flaskapp_imagename>`

Note: The container port should match the port number set under the Networking rules while creating the VM. 

## Connect to the app from your machine

Perform a request to `http://<public_ip_of_AzureVMinstance>:5000/endpoint` to check it works.

## Shut down the instance (IMPORTANT STEP!)

Go to the Virtual Machine overview in Azure Portal and click on Stop at the top. This will stop and deallocate the instance so that you will not be charged for this instance any longer.