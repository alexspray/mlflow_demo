# Steps to run a Flask app inside a Docker container and connect to a remote Mlflow server

## Requirements

- Mlflow server running on a computer (e.g. host machine of Docker or a remote computer)
- Flask app server running inside a Docker container

## Detailed description of the modules

### Mlflow server (on Host machine or a different computer)

The Mlflow server is started from the command line in the directory where the mlruns folder is located. This is done by the command:

```PowerShell
mlflow server --host '0.0.0.0'
```

Note: By setting the host IP address to '0.0.0.0', any server running on the local machine can be reached from other computers using its ipv4 address. By default, mlflow uses '127.0.0.1' or 'localhost' which means that the server can only be reached from the same computer but not from other machines (including the Docker container). While the host IP address is set as '0.0.0.0', we don't use this address to interact with the server (either by browser or through Requests). Instead, we use 'localhost' or '127.0.0.1' if connecting from the same machine or the local machine's ipv4 address (typically something like 192.168.x.x or 10.x.x.x or 172.x.x.x).

### Flask app server (on Docker container)

This module accepts POST requests in a chosen endpoint. Data is passed into this module as JSON as part of the POST request. The module chooses the best model from a set of available models hosted on a remote Mlflow server, makes a prediction and returns it as the response to the POST request.

Run this app on a chosen port number and map a Host port (something other than 5000 as Port 5000 is used for the Mlflow server) to this chosen container port number while running the Docker container.  

### Choosing the best model (on Docker container)

This can be done using an Mlflow client which interacts with the remote Mlflow server (the tracking uri corresponding to its address and port). The client sorts all the runs for the experiment ordered by a given metric and returns the best model.

The tracking uri should correspond to `http://host.docker.internal:5000` if the Mlflow server is running on the host machine on which the Docker container is running. `host.docker.internal` allows the Docker container to access the ip address of its host machine. Port 5000 is used here because this is the default port that the Mlflow server runs on.

Note: To access a server running on `<container_port>` in a Docker container from the Host machine, we use `http://localhost:<host_port>` as the address after mapping `<host_port>:<container_port>` using the `-p` flag in the Docker run command. On the other hand, to access a server running on `<host_port>` in the host machine from inside a Docker container running on this machine, we use `http://host.docker.internal:<host_port>` as the address inside the Docker container.

### Starting the Docker container

The Docker container is started by mounting the mlruns folder from the Host machine as a volume in the Docker container and mapping a chosen Host port (say, 5001) with the chosen container port on which the Flask app is running. 

```PowerShell
docker run -p 5001:5001 -v C:\path\to\mlruns:/mlruns docker_image_for_flask_app
```

This will mount the `C:\path\to\mlruns` folder as `/mlruns` on the Docker container which allows access to all the artifacts from inside the Docker container.

### Interacting with the Flask app using a client (from Host machine or another computer)

Make a POST request to the Flask app server on Port 5001 with the data provided as JSON to get the prediction from the best model as the response.

## Overall process

1. Start the Mlflow server in the mlruns folder on the Host machine with the host flag set as '0.0.0.0'. This will start the server on Port 5000.
2. Start the Docker container by mapping a Host port (say, 5001) to the container port (where the Flask app is set to run) and also mounting the mlruns folder as a volume on the Docker container. This will start the Flask app on the Docker container.
3. Interact with the Flask app by making POST requests to `http://localhost:5001` from the host machine.
  