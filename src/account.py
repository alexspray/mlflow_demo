class Account():
    def __init__(self, balance=0.0):
        # default to a balance of zero
        # negative balances represent debt
        # # make this a property
        self.balance = balance

        # # add interest rate

    def process_deposit(self, amount):
        self.balance += amount

    def process_withdrawal(self, amount):
        # don't allow the broker to go into debt
        # self.balance -= amount
        if self.balance >= amount:
            self.balance -= amount
        else:
            raise ValueError("Insufficient balance.")
