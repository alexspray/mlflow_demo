import requests

import numpy as np

from account import Account

class InsuranceClient(Account):
    def __init__(self, model=None, balance=0.0):
        super().__init__(balance)

        # # a second model can be trained to determine if insurance is worth
        # # buying or selling given other, competing broker's quotes
        self.model = model


    # giving multiple quotes is very ugly
    def get_quote(self, broker_uri, tender):
        url = broker_uri + r'get_quote/'
        res = requests.post(url, json=tender)

        return res.json()
    

    # # this is where the model should bid, offer, or pass
    def something_really_interesting(self, quote):
        order_type = ['bid', 'offer', 'nada']

        choice = np.random.choice(order_type)
        if choice != 'nada':
            # # the broker should really be storing the quotes it's given out,
            # # although appending the quote to the tender isn't too bad
            # process and return order
            order = quote
            order['type'] = choice

            return order
        return


    # # this function is very similar to Broker.process_order()
    # the insurance is assumed to be used when bought
    def execute_order(self, broker_uri, order):
        url = broker_uri + r'make_order/'
        res = requests.post(url, json=order)

        # wait for confirmation
        if res.text:
            if   order['type'] == 'offer': # insured by broker
                self.process_withdrawal(order['offer_premium'])
            elif order['type'] == 'bid':   # insured by client
                self.process_deposit(order['bid_premium'])
            else:
                raise ValueError("Incorrect order type.")  
    
        # return confirmation
        return "Successfully placed order."
    

    # this function is only called if someone defaults
    def process_claim(self, broker_uri, order):
        # build and fill json claim
        claim = {'net_claim': order['amount_insured']}

        url = broker_uri + r'make_claim/'
        res = requests.post(url, json=claim)

        # wait for confirmation
        if res.text:
            if   order['type'] == 'offer': # insured by broker
                self.process_deposit(order['amount_insured'])    # reimburse broker
            elif order['type'] == 'bid':   # insured by client
                self.process_withdrawal(order['amount_insured']) # claim from broker
            else:
                raise ValueError("Incorrect order type.")  

        # return confirmation
        return "Successfully made claim."
