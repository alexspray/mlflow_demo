import numpy as np

from account import Account


# # data logging should be added for models to use
# # include tenders, quotes, and client and broker balances and payments
class InsuranceBroker(Account):
    def __init__(self, model, balance=0.0, spread=1.0):
        super().__init__(balance)

        self.model  = model
        # default to a spread of one
        # # this should be dictated by the bid and offer premiums if we are
        # # competing with other brokers, which are determined by another
        # # model (see comments in get_quote())
        self.spread = spread


    def get_honest_premium(self, tender, y_hat):
        return y_hat * tender['amount_insured']

    def get_bid_premium(self,    tender, y_hat):
        return self.get_honest_premium(tender, y_hat) * (1 - self.spread/2) + 0.005

    def get_offer_premium(self,  tender, y_hat):
        return self.get_honest_premium(tender, y_hat) * (1 + self.spread/2) - 0.005    


    def get_quote(self, tender):
        # predict debt default probability
        y_hat = self.model.predict_proba([list(tender['details'].values())])[0][0]

        # # if competing with other brokers this should be determined by
        # # another model that uses probability of defaulting and our
        # # competitors prices to price our premiums
        # process and return quote
        response = tender
        response['bid_premium']   = self.get_bid_premium(tender,   y_hat)
        response['offer_premium'] = self.get_offer_premium(tender, y_hat)

        return response


    def process_order(self, order):
        # # if expanding this code, include a payment processor that transfers
        # # money between classes by returning the values safely, instead of
        # # calculating the payments inside the broker and clients
        # process payment and return confirmation
        if   order['type'] == 'offer': # insured by broker
            self.process_deposit(order['offer_premium'])
        elif order['type'] == 'bid':   # insured by client
            self.process_withdrawal(order['bid_premium'])
        else:
            raise ValueError("Incorrect order type.")  

        return "Successfully placed order."


    def process_claim(self, claim):
        # even though this is processed as a withdrawal the balance can
        # increase given the net claim is negative
        # process payment and return confirmation
        self.process_withdrawal(claim['net_claim'])

        return "Successfully made claim."
