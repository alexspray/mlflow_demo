import pickle
import json

from flask import Flask, request
from waitress import serve

# you need to import the class template, otherwise python won't know how to
# recreate the object from the serialization
from models.notebooks.superduper.train import Oracle

from broker import InsuranceBroker

if __name__ == '__main__':
    # the insurance broker hosts their services online
    app = Flask(__name__)

    # only load the model once
    with open(r'src/broker/models/best/model.pkl', 'rb') as f:
        model = pickle.load(f)
    # brokers can be initialised with different models
    broker = InsuranceBroker(model)


    @app.route('/')
    def home():
        return "Yes, I am a landing page. How could you tell?"


    @app.route('/get_quote', methods=['POST'])
    def request_quote():
        tender = request.json

        # the broker and client classes are significantly simplified by only
        # dealing with singular quotes, orders, and claims
        # quotes = []

        # for tender in tenders:
            # the tender was is not included in the response because the
            # client already knows that information, it's superfluous
            # quotes.append(broker.get_quote(tender))

        return json.dumps(broker.get_quote(tender))
    # # remove me
    @app.route('/get_quotes', methods=['POST'])
    def request_quotes():
        tenders = request.json
        quotes = []
        for tender in tenders:
            quotes.append(broker.get_quote(tender))
        return json.dumps(broker.get_quote(tenders))        


    @app.route('/make_order', methods=['POST'])
    def process_order():
        # make the summary here to separate the logic and logging
        order = request.json

        # build the json response skeleton
        summary = {
            'initial_broker_balance': broker.balance, 
            'final_broker_balance': None,
            'net_payment_to_broker': None
            # # superfluous, can add back if needed
            # 'paid_to_broker': None, 
            # 'paid_to_client': None
        }

        # process the orders
        # for order in orders:
        #     broker.process_order(order)
        broker.process_order(order)

        # fill and return the response 
        summary['final_broker_balance']  = broker.balance
        summary['net_payment_to_broker'] = summary['final_broker_balance'] - summary['initial_broker_balance']

        return json.dumps(summary)
    # # remove me
    @app.route('/make_orders', methods=['POST'])
    def process_orders():
        orders = request.json
        summary = {
            'initial_broker_balance': broker.balance, 
            'final_broker_balance': None,
            'net_payment_to_broker': None
        }
        for order in orders:
            broker.process_order(order)
        summary['final_broker_balance']  = broker.balance
        summary['net_payment_to_broker'] = summary['final_broker_balance'] - summary['initial_broker_balance']
        return json.dumps(summary)

    @app.route('/make_claim', methods=['POST'])
    def process_claim():
        # make the summary here to separate the logic and logging
        claim = request.json

        # build the json response skeleton
        summary = {
            "initial_broker_balance": broker.balance,
            "final_broker_balance": None,
            # # does the broker have this information?
            # "initial_client_balance": client.balance,
            # "final_client_balance": None,
            "net_claim_from_broker": None
            # # superfluous, can add back if needed
            # "claimed_from_broker": None,
            # "reimbursed_to_broker": None
        }

        # process the claims
        # for claim in claims:
        #     broker.process_claim(claim)
        broker.process_claim(claim)

        # fill and return the response 
        summary['final_broker_balance']  = broker.balance
        # # summary['final_client_balance']  = client.balance
        summary['net_claim_from_broker'] = summary['initial_broker_balance'] - summary['final_broker_balance']

        return json.dumps(summary)
    @app.route('/make_claims', methods=['POST'])
    def process_claims():
        claims = request.json
        summary = {
            "initial_broker_balance": broker.balance,
            "final_broker_balance": None,
            "net_claim_from_broker": None
        }
        for claim in claims:
            broker.process_claim(claim)
        summary['final_broker_balance']  = broker.balance
        summary['net_claim_from_broker'] = summary['initial_broker_balance'] - summary['final_broker_balance']
        return json.dumps(summary)


    # # TEMP ! ! !
    @app.route('/_reset',  methods=['GET', 'POST'])
    def reset():
        broker.balance = 0

        return {'balance': broker.balance}
    @app.route('/_spread', methods=['POST'])
    def spread():
        spread = request.json['spread']
        broker.spread  = spread

        return {'spread':  broker.spread}


    # serve the app using waitress
    # app.run(debug=True, port=5001)
    serve(app, port='5001') # host='0.0.0.0' when in a container
