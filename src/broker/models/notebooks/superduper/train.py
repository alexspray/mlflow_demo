import pickle

import numpy as np

# # replace me with a real model
class Oracle(object):
    """A person (such as a priestess) through whom a god was believed to speak."""
    def predict(self, X):
        """Predict the future.

        Args:
            X: Useless
        """
        predictions = [':D', 'D:']
        return list(np.random.choice(predictions, size=len(X)))


    def predict_proba(self, X):
        """Predict the future probabilistically.

        Args:
            X: Useless
        """
        return list(np.random.random(size=len(X)))


oracle = Oracle()

with open(r'src/broker/models/superduper.pkl', 'wb') as f:
    pickle.dump(oracle, f)
