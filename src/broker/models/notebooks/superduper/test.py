import sys
import pickle

import pandas as pd

from sklearn.metrics import accuracy_score

# you need to import the class template, otherwise python won't know how to
# recreate the object from the serialization
from train import Oracle

# # replace me with a real model
# load the model
with open(r'src/broker/models/superduper.pkl', 'rb') as f:
    model = pickle.load(f)

# load the data
df = pd.DataFrame({
    "definitely_real_predictors": [
        {
            "id": "27867",
            "amount_insured": 69334.0,
            "details": {
                "LIMIT_BAL": 70000.0,
                "SEX": 2.0,
                "EDUCATION": 1.0,
                "MARRIAGE": 1.0,
                "AGE": 25.0,
                "PAY_1": 0.0,
                "BILL_AMT1": 69334.0,
                "PAY_AMT1": 3000.0,
            }
        }
    ],
    "absolutely_not_made_up_targets": [
        ":D"
    ],
})

X_test = df['definitely_real_predictors']
y_test = df['absolutely_not_made_up_targets']

# make some predictions
y_hat_test       = model.predict(X_test)
y_hat_test_proba = model.predict_proba(X_test)

# print the report
print(y_hat_test, y_hat_test_proba)
print(accuracy_score(y_test, y_hat_test))
