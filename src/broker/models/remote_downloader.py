import mlflow
from pkg_resources import run_script
import os

def get_model():

    tracking_uri = r"http://127.0.0.1:5000"
    experiment_name = 'Insurance - Model 3'

    client = mlflow.tracking.MlflowClient(tracking_uri=tracking_uri)

    experiment_id = client.get_experiment_by_name(experiment_name).experiment_id
    runs          = client.search_runs(experiment_id, order_by=['metrics.prec_score'])

    best_run_id = runs[0].info.run_id
    print('from client: best run id is ', best_run_id)

    download_folder = '..\downloaded_model'

    client.download_artifacts(best_run_id, 'model', download_folder)

    model_uri = 'file:' + os.path.join(download_folder, 'model')
    model = mlflow.sklearn.load_model(model_uri)
        
    print(f'the best model found is {model} run id {best_run_id}')
        
    return model