import numpy  as np
import scipy  as sp
import pandas as pd

from client import InsuranceClient



if __name__ == "__main__":
    # simulation parameters
    NUM_TENDERS     =  1
    NUM_SIMULATIONS = 40

    broker_uri = 'http://127.0.0.1:5001/'

    # load all data once
    df = pd.read_csv(r'.\historical_data.csv')

    # initialise client
    client = InsuranceClient()

    log = []

    # main loop
    for i in range(NUM_SIMULATIONS):
        # use a subset of the data for each simulation
        df_sample = df.sample(NUM_TENDERS)

        X_cols = [
            'ID', 'LIMIT_BAL', 'SEX', 'EDUCATION', 'MARRIAGE', 'AGE',
            'PAY_1',     'PAY_2',     'PAY_3',     'PAY_4',     'PAY_5',     'PAY_6',
            'BILL_AMT1', 'BILL_AMT2', 'BILL_AMT3', 'BILL_AMT4', 'BILL_AMT5', 'BILL_AMT6',
            'PAY_AMT1',  'PAY_AMT2',  'PAY_AMT3',  'PAY_AMT4',  'PAY_AMT5',  'PAY_AMT6',
            'def_pay'
        ]
        X_sim = df[X_cols]

        y_cols = [
            'ID', 'def_pay'
        ]
        y_sim = df[y_cols]

        for j in range(len(df_sample)):
            # get quotes from the broker
            quote = client.get_quote(broker_uri, tender)
            # decide whether to make an order
            order = client.something_really_interesting(quote)

            if order:
                # make an order
                client.execute_order(broker_uri, order)

            # check if the person defaults
            default = 
        
        order_summary, claims_summary = client.buy_and_use_insurance(tenders_df, claims_df, broker_uri)

        summary_row = get_summary_row(sim, order_summary, claims_summary)
        if len(summary_df)==0:
            summary_df = summary_row
        else:
            summary_df = pd.concat([summary_df, summary_row])

    summary_df.to_html('summary.html')
