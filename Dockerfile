FROM ubuntu

RUN apt update && \
    apt upgrade -y

RUN apt install -y python3-dev python3-pip

COPY ["./requirements.txt", "/requirements.txt"]
RUN pip install -r requirements.txt

COPY ["resources/", "/resources/"]
COPY ["src/",       "/src/"]

ENV PYTHONPATH="./src/:./src/broker:./src/client"

WORKDIR /

CMD ["python3", "src/broker/app.py"]
# CMD ["python3", "src/simulate.py"]
